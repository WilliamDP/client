#!/usr/bin/python
# -*- coding: big5 -*-
import urllib2
import json
import time
import datetime

while True:
    url = "http://192.168.20.207:5000"
    now = '{0:%Y-%m-%d %H:%M:%S.%f}'.format(datetime.datetime.now())

    try:
        request = urllib2.Request(url)
        response = urllib2.urlopen(request)
        data = json.loads(response.read())

        sensorData = now + ', t:' + data['temperature'] + ', h:' + data['humidity']

        if data['state'] == 0:
            msg = sensorData + ', LED OFF'
        elif data['state'] == 1:
            msg = sensorData + ', LED ON'
        else:
            msg = now + ' 居然能判斷到這裡，快去買樂透'

        print msg

    except urllib2.HTTPError:
        print now + ' 你得到未知的問題，請舉手發問'
    except IOError:
        print now + ' 伺服器未上線'
    time.sleep(1)
